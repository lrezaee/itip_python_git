# -*- coding: utf-8 -*-
"""
Created on Thursday Oct 15 12:48:00 2020
Updated on Monday Nov 9, 2020

@author: lrezaee
"""

import os

patent_input_file_dir = os.path.join(os.path.abspath('.') , 'clean_patent_data_files')
#patent_input_file_dir = os.path.join(os.path.abspath('.') , 'test')
patents_file_dir = os.path.join(os.path.abspath('.') , 'patent_data')
                                      
in_path = str(patent_input_file_dir)
out_path = str(patents_file_dir)

#--------------
#STEP 1 : Merge excel files and select relevant data columns
''' 
Step 1: read patents

    Using the function itip_read_patent_data, the patent excel files are read
    and merged.
    
    The patent data are saved as a Pandas Dataframe and exported to an excel
    file which will be used by future files for ML algorithms.
    
'''
from itip_functions import itip_read_patent_data
original_patents_file = "raw_all_patent_data.xlsx"
itip_read_patent_data(in_path, out_path, original_patents_file)

#----------------------
#STEP 2 : Preprocess Patent Data
'''
Step 2: Read all patents and do stemming for claims, title, and abstract

'''
from itip_functions import itip_text_preprocess
input_filename = original_patents_file
in_path = str(patents_file_dir)
original_patents_file = "raw_all_patent_data.xlsx"
out_path = str(patents_file_dir)
output_train_stemmed_file = "patent_data_preprocessed_stemmed.xlsx"
itip_text_preprocess(in_path, input_filename, out_path, output_train_stemmed_file)



#--------
# Step 3: Read Test Patents
from itip_functions import itip_read_test_patents
patent_input_file_dir = os.path.join(os.path.abspath('.') , "test_patents")
patent_output_file_dir = os.path.join(os.path.abspath('.') , "patent_data")                      
in_path = str(patent_input_file_dir)
out_path = str(patent_output_file_dir)
input_test_file = "test_154_patents.xlsx"
original_test_patents = "clean_test_154_patents.xlsx"
itip_read_test_patents(in_path,out_path,input_test_file ,original_test_patents)


#STEP 4 : Preprocess Test Patent Data
from itip_functions import itip_text_preprocess
input_filename = "clean_test_154_patents.xlsx"
in_path = str(patents_file_dir)
out_path = str(patents_file_dir)
output_test_stemmed_file = "test_154_patents_preprocessed_stemmed.xlsx"
itip_text_preprocess(in_path, input_filename, out_path, output_test_stemmed_file)

#STEP 5: Similarity Calculation for test patents
from itip_functions import itip_similarity_calculation
in_path = str(patents_file_dir)
out_path = str(os.path.join(os.path.abspath('.') , 'similarity_results'))
input_train_filename = "patent_data_preprocessed_stemmed.xlsx"
input_test_filename = "test_154_patents_preprocessed_stemmed.xlsx"
output_file_name = "similarity_154_patents_preprocessed_stemmed.xlsx"
itip_similarity_calculation(in_path,input_train_filename, input_test_filename, out_path, output_file_name)


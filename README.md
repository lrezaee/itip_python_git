# ITIP - Inventory to IP 

## Project Definition
ITIP is a tool using machin learning algorithms to find patents matching an inventory device.

In the first phase, the reference for comparison is the text of the patents.

TfIdf is used to extract the keywords of the patents.

By vectorizing the text of the patent, such as Abstract, Claims, and Description, the similarity between patents are measured and scored.

A similarity matrix is made.

## Run the Code

* Steps to run the project:

1. Collect Patent Data : at this satge the patent data is collected from Innography
2. Data Preprocessing: To clean the data, remove duplicates, stemming, etc
3. Vectorize the refernce text of training patent set and extract the keywords
4. Vectorize the analyzed test patents based on the trained model and extracted keyword
5. Compute the similarity between the patents and find the score

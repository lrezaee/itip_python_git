# -*- coding: utf-8 -*-
"""
Created on Wed Oct 15 17:05:24 2020

This script includes the functions used in itip, including:
    itip_read_patent_data
    
        
@author: lrezaee
"""

import os 
import pandas as pd 
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
# from nltk.corpus import wordnet
import re
from nltk.corpus import stopwords


def itip_read_test_patents(input_path,output_path,filename,output_file):
    '''
    This function is to read all the excel files saved in the folder with the
    "input path" address, and read 13 columns and merge them together as
    a single file and as a Pandas DataFrame.
         
    '''
    df1 = pd.DataFrame()
    # All the patent data to be saved in the patent_Data dataframe
    
    if filename.endswith('.xlsx'):
        print('Now we are reading ' + filename)
        df1 = pd.read_excel(os.path.join(input_path, filename),
                              usecols=['Publication Number', 
                                       'Kind Code', 
                                       'Title', 
                                       'Abstract', 
                                       'First Claim',
                                       'All Claims',
                                       'All CP Classifications',
                                       'All IP Classifications',
                                       'All US Classifications',
                                       'Inventors',
                                       'Backward Citations',
                                       'Forward Citations'
                                       ],\
                                  dtype=str
                                  )
        # df1 is a dataframe read an excel file
        df1['Normalized_Current_Assignee'] = 'Unknown'
        df1['Original_filename'] = filename
        df1['Title_Abstract_Claims'] =  df1.Title + ' ' + \
            df1.Abstract + ' ' + df1['All Claims']
        # just to have a record, the interim results are saved as excel files
                              
    df1 = df1.astype(str)
    df1.to_excel(os.path.join(output_path,output_file))
    return 


#-----

def itip_read_patent_data(input_path,output_path, output_file_name):
    '''
    This function is to read all the excel files saved in the folder with the
    "input path" address, and read 13 columns and merge them together as
    a single file and as a Pandas DataFrame.
         
    '''
    patent_Data = pd.DataFrame()
    # All the patent data to be saved in the patent_Data dataframe
    
    for filename in os.listdir(input_path):
        if filename.endswith('.xlsx'):
            print('Now we are reading ' + filename)
            df1 = pd.read_excel(os.path.join(input_path, filename),
                              usecols=['Publication Number', 
                                       'Kind Code', 
                                       'Title', 
                                       'Abstract', 
                                       'First Claim',
                                       'All Claims',
                                       'All CP Classifications',
                                       'All IP Classifications',
                                       'All US Classifications',
                                       'Inventors',
                                       'Backward Citations',
                                       'Forward Citations'
                                       ],\
                                  dtype=str
                                  )
            # df1 is a dataframe read an excel file
            a = str(filename)
            df1['Normalized_Current_Assignee'] = \
                a[a.index('_')+1:a[a.index('_')+1:].index('_')+a.index('_')+1]
            # the assignee name is between the first two _ markers
            df1['Original_filename'] = filename
            
            df1['Title_Abstract_Claims'] =  df1.Title + ' ' + \
                df1.Abstract + ' ' + df1['All Claims']
            patent_Data = patent_Data.append(df1)
            
            # Just used for test to have a record of the interim results
            # saved as excel files
            #patent_Data.to_excel\
            #    (os.path.join(output_path,"patent_data_test_upto_" + \
            #                  str(filename)+".xlsx"))
                
    patent_Data = patent_Data.astype(str)
    patent_Data.to_excel(os.path.join(output_path,output_file_name))
    return patent_Data
#-----

def itip_text_preprocess(input_dir,input_file_name, output_dir, output_file_name):
    patent_DF = pd.DataFrame()
    filename = os.path.join(os.path.abspath('.'), input_dir,input_file_name)
    patent_DF = pd.read_excel(filename,\
                        usecols=[ 'Publication Number', 
                                  'Kind Code', 
                                  'Title', 
                                  'Abstract', 
                                  'First Claim',
                                  'All Claims',
                                  'Title_Abstract_Claims',
                                  'Normalized_Current_Assignee'
                                  ],\
                          )
    patent_DF.shape
    patent_DF = patent_DF.drop_duplicates()
    patent_DF.shape
    patent_DF = patent_DF.astype(str)
    from itip_functions import itip_to_stem
    patent_DF['Stemmed_text_Title_Abstract_Claims'] = [itip_to_stem(x) for x in 
                            patent_DF['Title_Abstract_Claims']]
    print('Title_Abstract_Claims are Stemmed.')
    patent_DF['Stemmed_text_Abstract'] = [itip_to_stem(x) for x in 
                                patent_DF['Abstract']]
    print('Abstracts are Stemmed.')
    patent_DF['Stemmed_text_Claims'] = [itip_to_stem(x) for x in 
                                patent_DF['All Claims']]
    print('Claims are Stemmed.')
    output_excel_file = os.path.join(os.path.abspath('.') , output_dir, \
                                     output_file_name)
    patent_DF.to_excel(output_excel_file)  
    
#-----

def itip_to_stem(parageraph):
    '''
    This function is to tokenize and stem the words in the input text.
    PorterStemmer is used for stemming
    '''
    token_words = word_tokenize(parageraph)
    stop_words = stopwords.words('English')
    stem_paragraph = []
    for word in token_words:
        if ((word[0].isdigit()) or (word in stop_words)):
            #or (word in punc_string)):
            #stem_paragraph.append('')
            continue
        else:
            word = re.sub('[!#?,.:";]', '',word)
            stem_paragraph.append(PorterStemmer().stem(word))
            #print(word)
        stem_paragraph.append(' ')
    return "".join(stem_paragraph)



def itip_similarity_calculation(input_dir,input_train_filename, test_filename, output_dir, output_file_name):
    import pandas as pd
    import os
    from sklearn.feature_extraction.text import TfidfVectorizer
#    from sklearn.feature_extraction.text import CountVectorizer
    patent_DF = pd.DataFrame()
    train_filename = os.path.join(os.path.abspath('.') , input_dir,input_train_filename)

    train_patent_DF = pd.read_excel(train_filename,\
                        usecols=[ 'Publication Number', 
                                  'Kind Code', 
                                  'Title', 
                                  'Abstract', 
                                  'First Claim',
                                  'All Claims',
                                  'Title_Abstract_Claims',
                                  'Normalized_Current_Assignee',
                                  'Stemmed_text_Title_Abstract_Claims',
                                  'Stemmed_text_Claims',
                                  'Stemmed_text_Abstract'
                                  ],
                                  )
    
    print('Train patent data file is read.')
    print('Size of the train file is ', train_patent_DF.shape)
    train_patent_DF = train_patent_DF.drop_duplicates()
    train_patent_DF.shape
    train_text = pd.DataFrame()
    train_text['T_A_Cls'] = train_patent_DF['Stemmed_text_Title_Abstract_Claims']
    train_text['Abs'] = train_patent_DF['Stemmed_text_Abstract']
    train_text['Cls'] = train_patent_DF['Stemmed_text_Claims']
     
    
    '''
    train_text['Reference'] = patent_DF['Publication Number']
    
    def create_text_matrix(target_text_input, vectorizer, category = 'T_A_Cls'):
        text_list = target_text_input[category]
        text_index = target_text_input['Reference']
        doc_term_matrix = vectorizer.fit_transform(text_list)
        return pd.DataFrame(doc_term_matrix.toarray(), index = text_index,
                     columns=vectorizer.get_feature_names())
    
    my_matrix_only_count_T_A_Cls = create_text_matrix(train_text,
                                                      CountVectorizer(), 
                                                      category = 'T_A_Cls' )
                
    keywords_counter_T_A_Cls = my_matrix_only_count_T_A_Cls.columns
    my_matrix_only_count_Abs = create_text_matrix(train_text,
                                                  CountVectorizer(), 
                                                  category = 'Abs' )
    keywords_counter_Abs = my_matrix_only_count_Abs.columns
    my_matrix_only_count_Cls = create_text_matrix(train_text,
                                                  CountVectorizer(), 
                                                  category = 'Cls' )
    keywords_counter_Cls = my_matrix_only_count_Cls.columns

    #----
    matrix_T_A_Cls_tfidf = create_text_matrix(train_text,
                                TfidfVectorizer(lowercase=True, max_df=0.85, 
                                          min_df=0.005), category = 'T_A_Cls')
    keywords_T_A_Cls_tfidf = matrix_T_A_Cls_tfidf.columns
    print(keywords_T_A_Cls_tfidf)
    matrix_T_A_Cls_tfidf.shape
    #----
    matrix_Abs_tfidf = create_text_matrix(train_text,
                               TfidfVectorizer(lowercase=True, max_df=0.85, 
                                           min_df=0.005), category = 'Abs')
    keywords_Abs_tfidf = matrix_Abs_tfidf.columns
    print(keywords_Abs_tfidf)
    matrix_Abs_tfidf.shape
    #----
    matrix_Cls_tfidf = create_text_matrix(train_text,
                               TfidfVectorizer(lowercase=True, max_df=0.85, 
                                               min_df=0.005), category = 'Cls')
    keywords_Cls_tfidf = matrix_Cls_tfidf.columns
    print(keywords_Cls_tfidf)
    matrix_Abs_tfidf.shape
        
    '''
    test_file = os.path.join(os.path.abspath('.') , input_dir,test_filename)
    test_patent_DF = pd.read_excel(test_file,\
                        usecols=[ 'Publication Number', 
                                  'Kind Code', 
                                  'Title', 
                                  'Abstract', 
                                  'First Claim',
                                  'All Claims',
                                  'Title_Abstract_Claims',
                                  'Normalized_Current_Assignee',
                                  'Stemmed_text_Title_Abstract_Claims',
                                  'Stemmed_text_Claims',
                                  'Stemmed_text_Abstract'
                                  ],\
                          )
    print('Test patent data file is read.')
    print('Size of the test data matrix is ', train_patent_DF.shape)
    print('*****************************************************************')
    test_patent_DF.shape
    test_text = pd.DataFrame()
    test_text['T_A_Cls'] = test_patent_DF['Stemmed_text_Title_Abstract_Claims']
    test_text['Abs'] = test_patent_DF['Stemmed_text_Abstract']
    test_text['Cls'] = test_patent_DF['Stemmed_text_Claims']
    tf_vec = TfidfVectorizer(lowercase=True, max_df=0.85, min_df=0.005)
    tf_vec.fit_transform(train_text['T_A_Cls'])
    tf_vec.get_feature_names()
    train_vect_all_Ab_T_Cl = tf_vec.transform(train_text['T_A_Cls'])
    test_Ab_T_Cl_vectorized = tf_vec.transform(test_text['T_A_Cls'])
    
    print('Text is vectorized')
    print('*****************************************************************')
    similarity_to_all = train_vect_all_Ab_T_Cl.dot(test_Ab_T_Cl_vectorized.T)
    similarity_matrix = pd.DataFrame(similarity_to_all.toarray(), \
                          index = train_patent_DF['Publication Number'],
                          columns=test_patent_DF['Publication Number'])
    similarity_over20_percent = (similarity_matrix>=0.20).astype(int)\
        *similarity_matrix*100
    similarity_over40_percent = (similarity_matrix>=0.40).astype(int)\
        *similarity_matrix*100
    similarity_over40_percent['sum']= similarity_over40_percent.sum(axis=1)
    similarity_over40_percent = similarity_over40_percent.\
        sort_values(by=['sum'],ascending=False)
    score_sum = similarity_over40_percent.sum(axis=0)
    score_sum.name = 'Score_Summation'
    similarity_over40_percent =similarity_over40_percent.append(score_sum)
    similarity_over40_percent_sorted = similarity_over40_percent.\
        sort_values(axis=1, by = 'Score_Summation',ascending=False)
            
    similarity_filename = os.path.join(os.path.abspath('.') , \
                                       output_dir, output_file_name)
    similarity_filename_over20 = os.path.join(os.path.abspath('.') , \
                                              output_dir, 'over_20_'
                                              + output_file_name)
    similarity_filename_over40_sorted = os.path.join(os.path.abspath('.'), \
                                        output_dir, 'sorted_over_40_'
                                        + output_file_name)
    
    similarity_matrix.to_excel(similarity_filename)
    similarity_over20_percent.to_excel(similarity_filename_over20)
    similarity_over40_percent_sorted.to_excel(similarity_filename_over40_sorted)
    
    
